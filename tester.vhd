Library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tester is end entity;

architecture behavioral of tester is
    Signal flipflops : STD_LOGIC_VECTOR(4 downto 0) := "00000";
    Signal clk: STD_LOGIC := '0';
begin
    s1: entity work.johnson_counter port map(clk,
            flipflops(0),
            flipflops(1),
            flipflops(2),
            flipflops(3),
            flipflops(4)
            );
    process
        variable output: String(1 to 5);
        variable i: Integer;
        variable j: Integer;
    begin
        j:=0;
        while j<20 loop
            clk<=not(clk);
            if clk='1' then
                i:=1;
                while i<=5 loop
                    if flipflops(i-1)='1' then
                        output(i):='1';
                    else
                        output(i):='0';
                    end if;
                    i:=i+1;
                end loop;
                report output;
            end if;
            wait for 50 ns;
            j:=j+1;
        end loop;
        wait;
    end process;
end architecture;
