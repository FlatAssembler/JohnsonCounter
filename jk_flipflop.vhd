library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity JK_FlipFlop is
    Port (J, K, clk: IN STD_LOGIC;
          Q, Qn: OUT STD_LOGIC);
end entity;

architecture behavioral of JK_FlipFlop is
    Signal current_state: STD_LOGIC := '0';
begin
    Process (J,K) Is
    begin
        If J='0' and K='1' Then
            current_state<='0';
        ElsIf J='1' and K='0' Then
            current_state<='1';
        ElsIf J='1' and K='1' Then
            current_state<=not(current_state);
        Else
            current_state<=current_state;
        End If;
    End process;
    Process (clk) Is
    begin
        If rising_edge(clk) Then
            Q<=current_state;
            Qn<=not(current_state);
        End if;
    end process;
end architecture;
