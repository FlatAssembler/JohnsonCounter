library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity johnson_counter is
    port(clk: in STD_LOGIC;
         b0,b1,b2,b3,b4: out STD_LOGIC);
end entity;

architecture structural of johnson_counter is
    signal q0 : STD_LOGIC := '0';
     signal qn0 : STD_LOGIC := '1';
     signal q1 : STD_LOGIC := '0';
     signal qn1 : STD_LOGIC := '1';
     signal q2 : STD_LOGIC := '0';
     signal qn2 : STD_LOGIC := '1';
     signal q3 : STD_LOGIC :='0';
     signal qn3 : STD_LOGIC :='1';
     signal q4 : STD_LOGIC :='0';
     signal qn4 : STD_LOGIC :='1';
begin
    s0: ENTITY work.jk_flipflop Port map(qn4,q4,clk,q0,qn0);
    s1: ENTITY work.jk_flipflop Port map(q0,qn0,clk,q1,qn1);
    s2: ENTITY work.jk_flipflop Port map(q1,qn1,clk,q2,qn2);
    s3: ENTITY work.jk_flipflop Port map(q2,qn2,clk,q3,qn3);
    s4: ENTITY work.jk_flipflop Port map(q3,qn3,clk,q4,qn4);
    b0 <= q0;
    b1 <= q1;
    b2 <= q2;
    b3 <= q3;
    b4 <= q4;
end architecture;
